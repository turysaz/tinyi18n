// This file is part of the tinyi18n library.
// Copyright (C) 2023 Turysaz
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package tinyi18n

import (
	"encoding/csv"
	"errors"
	"io"
)

type Translator struct {
	// The default language key.
	// This key is used if no language is specified or if
	// the text for the chosen language is an empty string.
	Default string

	// A translator has a map of maps of strings.
	// The first key is the language key, for example "en".
	// The second key is the resource identifier.
	// For example:
	//		t["en"]["myHWString"] == "Hello world"
	texts map[string]map[string]string
}

var (
	CorruptResourcesError = errors.New("Resources file seems to be corrupt!")
	DuplicateLangError    = errors.New("Duplicate language definition!")
	DuplicateKeyError     = errors.New("Duplicate resource definition!")
)

func NewTranslator(resources io.Reader) (Translator, error) {
	r := csv.NewReader(resources)
	r.TrimLeadingSpace = true

	langLine, err := r.Read()
	if err != nil {
		return Translator{}, err
	}

	if len(langLine) < 2 {
		// No lang column or no columns at all!
		return Translator{}, CorruptResourcesError
	}

	langs := make(map[string]map[string]string)
	langIndex := make(map[int]string) // map language key to column index

	for i, lang := range langLine[1:] { // skip the first column
		if _, exists := langs[lang]; exists {
			return Translator{}, DuplicateLangError
		}

		langs[lang] = make(map[string]string)
		langIndex[i] = lang
	}

	lines, err := r.ReadAll()
	if err != nil {
		return Translator{}, err
	}

	for _, line := range lines {
		key := line[0]
		for i, text := range line[1:] {
			lang := langs[langIndex[i]]

			if _, exists := lang[key]; exists {
				return Translator{}, DuplicateKeyError
			}

			lang[key] = text
		}
	}

	t := Translator{Default: langIndex[0], texts: langs}
	return t, nil
}

// Gets the translation for the language.
// If the language or the key is not found, an empty string
// and a 'false' value is returned.
// If the text is an empty string, the default language is
// chosen instead.
// If the t.Default is "", the fallback mechanism is deactivated.
func (t *Translator) TryGet(key, langKey string) (string, bool) {
	texts, ok := t.texts[langKey]
	if !ok {
		return "", false
	}

	text, ok := texts[key]
	if !ok {
		return "", false
	}

	// If the default is an empty string, the fallback to default
	// is deactivated.
	if t.Default == "" {
		return text, true
	}

	// If the text is empty, fall back the default.
	// (But only if this is not the default already!)
	if text == "" && langKey != t.Default {
		return t.TryGet(key, t.Default)
	}

	return text, true
}

// Gets the translation for the specified language.
// If either the language or the key is not found, an
// empty string is returned.
// If the resulting text is an empty string, the default language is
// chosen instead.
// If the t.Default is "", the fallback mechanism is deactivated.
func (t *Translator) Get(key, langKey string) string {
	text, _ := t.TryGet(key, langKey)
	return text
}

// Gets the translation for the default language.
// If the default is set to empty, an empty string is returned.
func (t *Translator) GetDefault(key string) string {
	return t.Get(key, t.Default)
}
