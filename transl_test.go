// This file is part of the tinyi18n library.
// Copyright (C) 2023 Turysaz
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package tinyi18n

import (
	"strings"
	"testing"
)

func compareStr(t *testing.T, expected, actual string) {
	if expected == actual {
		return
	}

	t.Errorf("Expected \"%v\", but found \"%v\".", expected, actual)
}

func compareBool(t *testing.T, expected, actual bool) {
	if expected == actual {
		return
	}

	t.Errorf("Expected \"%v\", but found \"%v\".", expected, actual)
}

func TestNewTranslator_HappyPath_ResourcesLoaded(t *testing.T) {
	csv := `,en,de
hw,"Hello World","Hallo Welt"
fruitMelon,"Water melon","Wassermelone"
`
	r := strings.NewReader(csv)
	sut, err := NewTranslator(r)

	if err != nil {
		t.Fatalf("Ctor returned err: %v", err)
	}

	compareStr(t, "Hello World", sut.Get("hw", "en"))
	compareStr(t, "Hallo Welt", sut.Get("hw", "de"))
	compareStr(t, "Water melon", sut.Get("fruitMelon", "en"))
	compareStr(t, "Wassermelone", sut.Get("fruitMelon", "de"))
}

func TestNewTranslator_Indentation_Trimmed(t *testing.T) {
	csv := `,en,de
		"hw",			"Hello World",	"Hallo Welt"
		"withLeading",	"   A",			"   B"
		"fruitMelon", 	"Water melon",	"Wassermelone"
`
	r := strings.NewReader(csv)
	sut, err := NewTranslator(r)

	if err != nil {
		t.Fatalf("Ctor returned err: %v", err)
	}

	compareStr(t, "Hello World", sut.Get("hw", "en"))
	compareStr(t, "Hallo Welt", sut.Get("hw", "de"))
	compareStr(t, "   A", sut.Get("withLeading", "en"))
	compareStr(t, "   B", sut.Get("withLeading", "de"))
	compareStr(t, "Water melon", sut.Get("fruitMelon", "en"))
	compareStr(t, "Wassermelone", sut.Get("fruitMelon", "de"))
}

func TestNewTranslator_EmptyLines_Trimmed(t *testing.T) {
	csv := `,en,de


		a,a,a


		b,b,b`
	r := strings.NewReader(csv)
	sut, err := NewTranslator(r)

	if err != nil {
		t.Fatalf("Ctor returned err: %v", err)
	}

	compareStr(t, "a", sut.Get("a", "en"))
	compareStr(t, "a", sut.Get("a", "de"))
	compareStr(t, "b", sut.Get("b", "en"))
	compareStr(t, "b", sut.Get("b", "de"))
}

func TestNewTranslator_NoLanguages_CorruptError(t *testing.T) {
	csv := `
hw
fruitMelon
`
	r := strings.NewReader(csv)
	_, err := NewTranslator(r)

	expected := CorruptResourcesError

	if err != expected {
		t.Fatalf(
			"Expected Error '%v', but found '%v'",
			expected,
			err)
	}
}

func TestNewTranslator_InconsistentLineCount_CorruptError(t *testing.T) {
	csv := `
hw,abc
fruitMelon
`
	r := strings.NewReader(csv)
	_, err := NewTranslator(r)

	if err == nil {
		t.Fatal("Expected error!")
	}
}

func TestNewTranslator_DuplicateLang_DuplicateError(t *testing.T) {
	csv := `,en,en
a,foo,bar
b,foo,bar
`
	r := strings.NewReader(csv)
	_, err := NewTranslator(r)

	expected := DuplicateLangError

	if err != expected {
		t.Fatalf(
			"Expected Error '%v', but found '%v'",
			expected,
			err)
	}
}

func TestNewTranslator_DuplicateKey_DuplicateError(t *testing.T) {
	csv := `,en,de
a,foo,bar
a,foo,bar
`
	r := strings.NewReader(csv)
	_, err := NewTranslator(r)

	expected := DuplicateKeyError

	if err != expected {
		t.Fatalf(
			"Expected Error '%v', but found '%v'",
			expected,
			err)
	}
}

func TestTryGet_KeyNotFound_EmptyAndFalse(t *testing.T) {
	csv := `,en,de
a,aEN,aDE
b,bEN,bDE
`
	r := strings.NewReader(csv)
	sut, err := NewTranslator(r)

	if err != nil {
		t.Fatalf("Ctor failed: %v", err)
	}

	text, ok := sut.TryGet("c", "de")
	compareStr(t, "", text)
	compareBool(t, false, ok)
}

func TestTryGet_LangNotFound_EmptyAndFalse(t *testing.T) {
	csv := `,en,de
a,aEN,aDE
b,bEN,bDE
`
	r := strings.NewReader(csv)
	sut, err := NewTranslator(r)

	if err != nil {
		t.Fatalf("Ctor failed: %v", err)
	}

	text, ok := sut.TryGet("a", "fr")
	compareStr(t, "", text)
	compareBool(t, false, ok)
}

func TestTryGet_ResultForDefaultEmpty_EmptyAndTrue(t *testing.T) {
	csv := `,en,de
a,,aDE
b,bEN,bDE
`
	r := strings.NewReader(csv)
	sut, err := NewTranslator(r)

	if err != nil {
		t.Fatalf("Ctor failed: %v", err)
	}

	text, ok := sut.TryGet("a", "en")
	compareStr(t, "", text)
	compareBool(t, true, ok)
}

func TestTryGet_ResultForNonDefaultEmpty_DefaultAndTrue(t *testing.T) {
	csv := `,en,de
a,,aDE
b,bEN,bDE
`
	r := strings.NewReader(csv)
	sut, err := NewTranslator(r)

	if err != nil {
		t.Fatalf("Ctor failed: %v", err)
	}

	sut.Default = "de"

	text, ok := sut.TryGet("a", "en")
	compareStr(t, "aDE", text)
	compareBool(t, true, ok)
}

func TestTryGet_ResultEmptyAndNoDefault_EmptyAndTrue(t *testing.T) {
	csv := `,en,de
a,,aDE
b,bEN,bDE
`
	r := strings.NewReader(csv)
	sut, err := NewTranslator(r)

	if err != nil {
		t.Fatalf("Ctor failed: %v", err)
	}

	sut.Default = ""

	text, ok := sut.TryGet("a", "en")
	compareStr(t, "", text)
	compareBool(t, true, ok)
}
