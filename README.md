
# Tiny I18N

This is a tiny i18n library that takes texts from a CSV file, where each
column represents another language.


## Usage

### 1. Create a resource file like this:

    key,            en,         de
    
    button:save,    Save,       Speichern
    button:close,   Close,      Schließen
    button:abort,   Abort,      Abbrechen

    header:options, Options,    Optionen

... and so on.


### 2. In your code, create and use a new Translator

```golang
package main

import (
    "fmt"
    "log"
    "os"

    i18n "codeberg.org/turysaz/tinyi18n"
)

func main() {

    texts, err := os.Open("my-resources.csv")
    if err != nil {
        log.Fatal(err)
    }
    defer texts.Close()

    translator, err := i18n.NewTranslator(texts)
    if err != nil {
        log.Fatal(err)
    }

    translator.Default = "de"

    fmt.Println(translator.GetDefault("button:save")) // "Speichern"
    fmt.Println(translator.Get("button:save", "en"))  // "Save"

    result, ok := translator.TryGet("foobar", "de") 
    if ok {
        fmt.Println(result)
    } else {
        fmt.Println("Text not found")
    }
}
```


